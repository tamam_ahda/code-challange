"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     */

    await queryInterface.bulkInsert("tasks", [
      {
        users_id: 1,
        description: "Pekerjaan pertama",
        status: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        users_id: 2,
        description: "Pekerjaan kedua",
        status: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        users_id: 3,
        description: "Pekerjaan ketiga",
        status: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        users_id: 4,
        description: "Pekerjaan keempat",
        status: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     */
    await queryInterface.bulkDelete('tasks', null, {});
  },
};
