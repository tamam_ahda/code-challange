const usersController = require("./user-controller");
const taskController = require("./task-controller")

class Controller {
    users() {
        return usersController
    }
    tasks() {
        return taskController
    }
}

module.exports = Object.freeze(new Controller())