const db = require("../models");

class TasksController {
  async get(req, res) {
    const userModel = db.users;
    const taskModel = db.tasks;

    userModel.hasMany(taskModel);
    taskModel.belongTo(userModel, { foreignkey: "users_id" });

    const data = await db.taskModel.findAll({
      attributes: ["id", "description", "status"],
      include: [
        {
          model: userModel,
          attributes: ["full_name"],
        },
      ],
    });
    res.json(data);
  }
  async get(req, res) {
    const data = await db.tasks.findAll({
      attributes: ["users_id", "description", "status"],
    });
    res.json(data);
  }
  async post(req, res) {
    const userId = req.body["users_id"];
    const description = req.body["description"];
    await db.tasks.create({
      users_id: userId,
      description: description,
      status: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
    res.send("success insert data " + userId);
  }
  async put(req, res) {
    const description = req.body["description"];
    const status = req.body["status"];
    const id = req.body["id"];
    await db.tasks.update(
      { description: description, status: status },
      { where: { id: id } }
    );
    res.send(`success update status ${status} with id  ${id}`);
  }
  async delete(req, res) {
    const id = req.query.id;
    await db.tasks.destroy({ where: { id: id } });
    res.send("success delete from tasks with id " + id);
  }
}

module.exports = Object.freeze(new TasksController());
